package lambda_task1;

import java.util.Comparator;
import java.util.stream.Stream;

public class Main {

    public static void main(String[] args) {
        CustomInterface returnMax = (a, b, c) -> ( (a >= b) && (a >= c) ? a :
                                                    (b >= a) && (b >= c) ? b:
                                                            (c >= a) && (c >= b) ? c: 0);

        CustomInterface returnMax1 = (a, b, c) -> (Stream.of(a, b, c)
                .max(Comparator.naturalOrder()).get());

        CustomInterface average = (a, b, c) -> ((a + b + c) / 3);

        System.out.println(returnMax.customFunction(12, 6, 2));
        System.out.println(returnMax1.customFunction(12, 6, 2));
        System.out.println(average.customFunction(1, 2, 3));
    }

}
