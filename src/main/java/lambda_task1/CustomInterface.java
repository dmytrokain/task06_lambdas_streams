package lambda_task1;

@FunctionalInterface
public interface CustomInterface {
    public int customFunction(int a, int b, int c);
}
