package task3Application;

import java.util.*;
import java.util.stream.Collectors;

public class StreamList {

    public static int numberUnique(String[] strings) {
        return (int) Arrays.stream(strings)
                .distinct()
                .count();
    }

    public static ArrayList sortedList(String[] strings) {
        return (ArrayList) Arrays.stream(strings)
                .sorted()
                .collect(Collectors.toList());
    }

    public static Map groupNumber(String[] strings) {
        return  Arrays.stream(strings)
                .collect(Collectors.groupingBy(String::length));
    }

    public static void main(String[] args) {
        ArrayList<Integer> list = new ArrayList<>(Arrays.asList(1, 2, 3, 3, 2, 6, 8));
        String [] strings = {"dec", "dic", "Docc", "Ee", "EEe", "g"};

        int number = numberUnique(strings);

        double average = list.stream()
                .mapToInt(Integer::intValue)
                .average().getAsDouble();

        int min = list.stream()
                .mapToInt(Integer::intValue)
                .min().getAsInt();

        int max = list.stream()
                .mapToInt(Integer::intValue)
                .max().getAsInt();

        int bigger_than_average = (int) list.stream()
                .filter(a -> a > average)
                .count();

        System.out.println(number);
        System.out.println(sortedList(strings));
        System.out.println(groupNumber(strings));
    }
}

