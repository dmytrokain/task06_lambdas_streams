package CommandPattern;

@FunctionalInterface
public interface Command {
    public void execute();
}
