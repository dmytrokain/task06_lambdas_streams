package CommandPattern;

public class Main {

    public static void main(String[] args) {

        Controller controller = new Controller();

        controller.addOperations(() -> {
            System.out.println("added first command");
        });

        controller.addOperations(() -> {
            System.out.println("added second command");
        });

        controller.run();
    }
}
