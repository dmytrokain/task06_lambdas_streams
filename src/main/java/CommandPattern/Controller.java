package CommandPattern;

import java.util.ArrayList;
import java.util.List;

public class Controller {

    public ArrayList<Command> operations
            = new ArrayList<>();

    public void addOperations(Command command) {
        operations.add(command);
    }

    public void run() {
        operations.stream()
                .forEach(Command::execute);
    }

    public static void main(String[] args) {
        Controller controller = new Controller();

        controller.addOperations(new TurnOnLight());
        controller.addOperations(new TurnOfLight());

        controller.run();
    }
}
