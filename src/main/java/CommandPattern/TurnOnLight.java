package CommandPattern;

public class TurnOnLight implements Command {
    @Override
    public void execute() {
        System.out.println("on");
    }
}
